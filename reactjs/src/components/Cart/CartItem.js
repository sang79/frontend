import React, { Component } from 'react';
export default class CartItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            quantity: 1,
            idProduct: '',
            idDelete: '',
            idSubtract: '',
            total: '',
            price: '',
            product: {}
        }
        this.handleSubtract = this.handleSubtract.bind(this)
        this.handleInput = this.handleInput.bind(this)
        this.handlePlus = this.handlePlus.bind(this)
        this.renderCart = this.renderCart.bind(this)
        this.deleteProduct = this.deleteProduct.bind(this)
    }
    componentDidMount() {
        let quantity = this.props.product.qty
        let price = this.props.product.price
        this.setState({
            price,
            quantity,
            total: price * quantity
        })
    }
    componentWillReceiveProps() {
        let quantity = this.props.product.qty
        let price = this.props.product.price
        this.setState({
            price,
            quantity,
            total: price * quantity
        })
    }
    handleInput(e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameInput]: value
        })
    }
    deleteProduct(e) {
        e.preventDefault();
        let idDelete = e.target.id
        let productCart = JSON.parse(localStorage.getItem('localCart'))
        this.props.deleteProduct(idDelete)
        delete productCart[idDelete];
        localStorage.setItem('localCart', JSON.stringify(productCart))
    }
    handlePlus(e) {
        e.preventDefault();
        let { quantity, total, price } = this.state
        let idProduct = this.props.product.id
        console.log(price)
        quantity = quantity + 1
        this.setState({ quantity })
        this.setState({ total: quantity * price })
        var getLocalCart = JSON.parse(localStorage.getItem('localCart'));
        Object.keys(getLocalCart).map((key, i) => {
            if (key == idProduct) {
                getLocalCart[key] = getLocalCart[key] + 1
            }
        })
        localStorage.setItem('localCart', JSON.stringify(getLocalCart))
    }
    handleSubtract(e) {
        e.preventDefault()
        let { quantity, total, price } = this.state
        let idProduct = this.props.product.id
        quantity = quantity - 1
        this.setState({ quantity })
        this.setState({ total: quantity * price })
        var getLocalCart = JSON.parse(localStorage.getItem('localCart'));
        Object.keys(getLocalCart).map((key, i) => {
            if (key == idProduct) {
                if (getLocalCart[key] > 1) {
                    getLocalCart[key] = getLocalCart[key] - 1
                } else {
                    this.props.subtract(idProduct)
                    delete getLocalCart[idProduct];
                    localStorage.setItem('localCart', JSON.stringify(getLocalCart))
                }
            }
        })
        localStorage.setItem('localCart', JSON.stringify(getLocalCart))
    }
    renderCart() {
        let { total } = this.state
        let { product } = this.props
        let img = JSON.parse(product.image);
        return (
            <tr>
                <td className="cart_product">
                    <img style={{ width: "100px" }} src={"http://localhost/laravel/laravel/public/upload/user/product/" + product.id_user + "/" + img[0]} />
                </td>
                <td className="cart_description">
                    <h4><a href="">{product.name}</a></h4>
                    <p>Web ID: {product.id}</p>
                </td>
                <td className="cart_price">
                    <p>{"$" + product['price']}</p>
                </td>
                <td className="cart_quantity">
                    <div className="cart_quantity_button">
                        <a className="cart_quantity_up" href="#" onClick={this.handlePlus} > + </a>
                        <input className="cart_quantity_input" type="text" name="quantity" value={this.state.quantity} onChange={this.handleInput} autocomplete="off" size="2" />
                        <a className="cart_quantity_down" href="#" onClick={this.handleSubtract}> - </a>
                    </div>
                </td>
                <td className="cart_total">
                    <p className="cart_total_price">{'$' + total}</p>
                </td>
                <td className="cart_delete">
                    <a className="cart_quantity_delete" id={product.id} onChange={this.handleInput} onClick={this.deleteProduct}>DELETE</a>
                </td>
            </tr>
        )

    }
    render() {
        return (
            <>
                {this.renderCart()}
            </>

        )
    }
}
