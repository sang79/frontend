import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './components/Layout/Header';
import MenuLeft from './components/Layout/MenuLeft';
import Footer from './components/Layout/Footer';
import BlogDetail from './components/Blog/BlogDetail'
class App extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let pathname = this.props.location.pathname

    return (
      <>
        <Header />
        <section>
          <div className="container">
            <div className="row">
              {(pathname.includes('account') || pathname.includes('cart')) ? '' : <MenuLeft />}
              {this.props.children}
            </div>
          </div>
        </section>
        <Footer />
      </>
      

    );
  }
}

export default withRouter(App);
