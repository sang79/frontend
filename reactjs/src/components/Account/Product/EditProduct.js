import React, { Component } from 'react';
import FormErrors from '../FormErrors'
import axios from 'axios';
export default class EditProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isChecked: false,
            checkedList: [],
            image: '', status: '', sale: '', detail: '', name: '', price: '', company: '', category: {}, brand: {}, files: [], imgFileName: [], imgFileType: [], formErrors: {},
        }
        this.handleInput = this.handleInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUserInputFile = this.handleUserInputFile.bind(this)
        this.renderCategory = this.renderCategory.bind(this)
        this.renderBrand = this.renderBrand.bind(this)
        this.renderSale = this.renderSale.bind(this)
        this.renderImages = this.renderImages.bind(this)
        this.handleCheck = this.handleCheck.bind(this)
    }
    componentDidMount() {
        console.log(this.props)
        let paramId = this.props.match.params.id;
        let userData = JSON.parse(localStorage.getItem('userData'));
        let accessToken = userData.loginToken['token'];
        let url = 'http://localhost/laravel/laravel/public/api/user/product/' + paramId
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application / x - www - form - urlencoded',
                'Accept': 'application / json'
            }
        };
        axios.get(url, config)
            .then(res => {
                console.log(res.data)
                this.setState({
                    ListProduct: res.data.data.image,
                    name: res.data.data.name,
                    price: res.data.data.price,
                    category: res.data.data.id_category,
                    brand: res.data.data.id_brand,
                    detail: res.data.data.detail,
                    status: res.data.data.status
                })
            })
            .catch(error => console.log(error))
        axios.get('http://localhost/laravel/laravel/public/api/category-brand')
            .then(res => {
                this.setState({
                    categoryList: res.data.category,
                    brandList: res.data.brand
                })
            })
            .catch(error => { console.log(error) })
    }
    renderImages() {
        let EditListProduct = this.state.ListProduct
        if (EditListProduct) {
            return Object.keys(EditListProduct).map((value, key) => {
                let userData = JSON.parse(localStorage.getItem('userData'));
                let id = userData.loginAuth.id
                return (
                    <li key={key} index={key} style={{ display: 'inline-block' }}>
                        <img style={{ width: "70px" }} src={"http://localhost/laravel/laravel/public/upload/user/product/" + id + "/" + EditListProduct[value]} />
                        <input name='AvatarCheckBox[]' style={{ width: '13px', margin: '10px', display: 'inline-block' }} type="checkbox" index={key} onChange={this.handleCheck} value={EditListProduct[value]} />
                    </li>
                )
            })
        }
    }
    handleCheck(e) {
        let index;
        let checkedList = this.state.checkedList
        const checked = e.target.checked;
        this.setState({ isChecked: !this.state.isChecked, checkedList: this.state.checkedList })
        if (checked) {
            checkedList.push(e.target.value)
        } else {
            index = checkedList.indexOf(checked)
            checkedList.splice(index, 1)
        }
        this.setState({checkedList})
        console.log(checkedList)
    }
    renderCategory() {
        let category = this.state.categoryList
        if (category) {
            return Object.keys(category).map((key, i) => {
                return (
                    <option value={category[key]['id']}>{category[key]['category']}</option>
                )
            })
        }
    }
    renderBrand() {
        let brand = this.state.brandList
        if (brand) {
            return Object.keys(brand).map((key, i) => {
                return (
                    <option key={i} value={brand[key]['id']}>{brand[key]['brand']}</option>
                )
            })
        }
    }
    handleInput(e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameInput]: value
        })
    }
    handleUserInputFile(e) {
        const files = e.target.files
        console.log(files)
        this.setState({
            files: files
        })
    }
    renderSale() {
        let status = this.state.status;
        if (status == 2) {
            return (
                <input type="number" style={{ width: '40%' }} value = {this.state.status}  name="percent" onChange={this.handleInput} />
            )
        }
    }
    handleSubmit(e) {
        let checkedList = this.state.checkedList
        console.log(checkedList.length)
        e.preventDefault();
        let userData = JSON.parse(localStorage.getItem('userData'));
        let paramId = this.props.match.params.id;
        let image = this.state.files
        let { name, price, company, sale, detail, status, category, brand, imgFileName, imgFileType, files, isChecked } = this.state
        let flag = true;
        let signInErr = this.state.formErrors
        this.setState({ imgFileName, imgFileType })
        if (files) {
            Object.keys(files).map((key, i) => {
                imgFileName.push(files[key].type)
                let type = files[key].type
                if (!type.match(/.(jpg|jpeg|png|gif)$/i)) {
                    flag = false
                    signInErr.files = "not image!"
                } else {
                    signInErr.files = ""
                }
            })
            if((this.state.ListProduct.length - this.state.checkedList.length) +(this.state.files.length) > 3) {
                flag = false
                signInErr.files = "too much!"
            }
        }
        if (!flag) {
            this.setState({ formErrors: signInErr })
        } else {
            this.setState({ formErrors: [] })
            let url = 'http://localhost/laravel/laravel/public/api/user/edit-product' + "/" + paramId
            let accessToken = userData.loginToken['token'];
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            let formData = new FormData();
            formData.append('name', this.state.name);
            formData.append('price', this.state.price);
            formData.append('category', this.state.category);
            formData.append('brand', this.state.brand);
            formData.append('company', this.state.company);
            formData.append('detail', this.state.detail);
            formData.append('status', this.state.status);
            formData.append('sale', this.state.sale);
            formData.append('avatarCheckBox[]', this.state.checkedList);
            Object.keys(image).map((Obj, i) => {
                formData.append("file[]", image[Obj])
            });
            axios.post(url, formData, config)
                .then(res => {
                    console.log(res.data)
                    this.props.history.push('/account/product/list')
                })
                .catch(error => { console.log(error) })
        }
    }
    render() {
        return (
            <div className="col-sm-4">
                <div className="signup-form">
                    <h2>Edit Product</h2>
                    <FormErrors formErrors={this.state.formErrors} />
                    <form action="#" onSubmit={this.handleSubmit} enctype="multipart/form-data">
                        <input type="text" name="name" value={this.state.name} onChange={this.handleInput} />
                        <input type="number" value={this.state.price} name="price" onChange={this.handleInput} />
                        <select name="category" value={this.state.category} onChange={this.handleInput}>
                            <option value="0">please choose category</option>
                            {this.renderCategory()}
                        </select>
                        <select name="brand" onChange={this.handleInput} value={this.state.brand}>
                            <option value="0">please choose brand</option>
                            {this.renderBrand()}
                        </select>
                        <select name="status" value = {this.state.status}  onChange={this.handleInput}>
                            <option value="0">please choose status</option>
                            <option value="1">new</option>
                            <option value="2">sale</option>
                        </select>
                        {this.renderSale()}
                        <input type="file" name="files[]" multiple onChange={this.handleUserInputFile} />
                        <ul>
                            {this.renderImages()}
                        </ul>
                        <textarea name="detail" onChange={this.handleInput} value={this.state.detail} />
                        <button type="submit" className="btn btn-default">Edit</button>
                    </form>
                </div>
            </div>
        )
    }
}
