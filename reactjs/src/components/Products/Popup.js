import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {
  PopupboxContainer,
  PopupboxManager
} from 'react-popupbox';
import "react-popupbox/dist/react-popupbox.css"
export default class PopUp extends Component {
  constructor(props) {
    super(props)
    this.openPopupbox = this.openPopupbox.bind(this)
  }
  openPopupbox() {
    let data = this.props.popData
    const content = <img src={data} />
    PopupboxManager.open({
      content,
      config: {
        titleBar: {
          enable: true
        },
        fadeIn: true,
        fadeInSpeed: 500
      }
    })
  }

  render() {
    return (
      <>
        <Link onClick={this.openPopupbox}><h3>ZOOM</h3></Link>
        <PopupboxContainer />
      </>
    )
  }
}
