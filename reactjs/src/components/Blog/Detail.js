import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom'
import Comment from './Comment';
import ListComment from './ListComment';
import Rate from './Rate';
class Detail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            IdReplay: '',
            idCmt:'',
            details: {},
            listCmt: []
        }
        this.getComment = this.getComment.bind(this)
        this.getIdReplay = this.getIdReplay.bind(this)
    }
    getComment(data) {
        this.setState({ listCmt: this.state.listCmt.concat(data)})
        console.log(data)
    }
    getIdReplay(dataId) {
        let IdReplay = dataId
        this.setState({IdReplay})
        console.log(IdReplay)
    }
    componentDidMount() {
        let info = this.props
        let paramId = this.props.match.params.id;
        axios.get('http://localhost/laravel/laravel/public/api/blog/detail/' + paramId)
            .then(res => {
                const details = res.data.data
                console.log(details)
                this.setState({
                    details,
                    listCmt: res.data.data.comment,
                });
            })
            .catch(error => console.log(error))
    }
    renderDetail() {
        let getDetail = this.state.details
        return (
            <>
                <div className="blog-post-area">
                    <h2 className="title text-center">Latest From our Blog</h2>
                    <div className="single-blog-post">
                        <div className="post-meta">
                            <ul>
                                <li><i className="fa fa-user"></i> Mac Doe</li>
                                <li><i className="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i className="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <span>
                                <i className="fa fa-star"></i>
                                <i className="fa fa-star"></i>
                                <i className="fa fa-star"></i>
                                <i className="fa fa-star"></i>
                                <i className="fa fa-star-half-o"></i>
                            </span>
                        </div>
                        <Link to="">
                            <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/" + getDetail['image']} alt="" />
                        </Link>
                        <p>{getDetail['description']}</p>
                        <div className="pager-area">
                            <ul className="pager pull-right">
                                <li><Link to="#">Pre</Link></li>
                                <li><Link to="#">Next</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="socials-share">
                    <Link to=""><img src="/../frontend/images/blog/blog-one.jpq" alt="" /></Link>
                </div>
            </>
        )
    }
    render() {
        return (
            <div className="col-sm-9">
                {this.renderDetail()}
                <Rate details={this.props} />
                <ListComment listCmt={this.state.listCmt} IdReplay = {this.getIdReplay} />
                <Comment listCmt={this.getComment} idCmt = {this.state.IdReplay} />
            </div>
        )
    }
}
export default Detail;






