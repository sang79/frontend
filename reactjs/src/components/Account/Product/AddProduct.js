import React, { Component } from 'react';
import FormErrors from '../FormErrors';
import axios from 'axios';
export default class AddProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
           image: '', status: '', sale: '', detail: '', name: '', price: '', company: '', category: {},categoryList: {}, brandList: {}, brand: {}, files: [], imgFileName: [], imgFileType: [], formErrors: {}
        }
        this.handleInput = this.handleInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUserInputFile = this.handleUserInputFile.bind(this)
        this.renderCategory = this.renderCategory.bind(this)
        this.renderBrand = this.renderBrand.bind(this)
        this.renderSale = this.renderSale.bind(this)
    }
    componentDidMount() {
        axios.get('http://localhost/laravel/laravel/public/api/category-brand')
            .then(res => {
                console.log(res.data)
                this.setState({
                    categoryList: res.data.category,
                    brandList: res.data.brand
                })
            })
            .catch(error => { console.log(error) })
    }
    renderCategory() {
        let category = this.state.categoryList
        if (category) {
            return Object.keys(category).map((key, i) => {
                return (
                    <option key={category['id']} value={category[key]['id']}>{category[key]['category']}</option>
                )
            })
        }
    }
    renderBrand() {
        let brand = this.state.brandList
        if (brand) {
            return Object.keys(brand).map((key, i) => {
                return (
                    <option key={brand['id']} value={brand[key]['id']}>{brand[key]['brand']}</option>
                )
            })
        }
    }
    handleInput(e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameInput]: value
        })
    }
    handleUserInputFile(e) {
        const files = e.target.files
        console.log(files)
        this.setState({files})
    }
    renderSale() {
        let status = this.state.status;
        if (status == 2) {
            return (
                <input type="number" style={{ width: '40%' }} placeholder="0" name="percent" onChange={this.handleInput} />
            )
        }
    }
    handleSubmit(e) {
        e.preventDefault();   
        let userData = JSON.parse(localStorage.getItem('userData'));
        let image = this.state.files
        let { name, price, company, sale, detail, status, category, brand, imgFileName, imgFileType, files } = this.state
        let flag = true;
        let signInErr = this.state.formErrors
        this.setState({ imgFileName, imgFileType })

        if (!name) {
            flag = false
            signInErr.name = 'enter name'
        } else {
            signInErr.name = ""
        }
        if (!price) {
            flag = false
            signInErr.price = "enter price"
        } else {
            signInErr.price = ""
        }
        if (!company) {
            signInErr.company = 'please text company!'
        } else {
            signInErr.company = ''
        }
        if (!detail) {
            signInErr.detail = 'please text detail!'
        } else {
            signInErr.detail = ''
        }
        if(category == '') {
            flag = false
            signInErr.category = 'please pick category!'
        } else {
            signInErr.category = ''
        }
        if(brand == 0) {
            flag = false
            signInErr.brand = 'please pick brand!'
        } else {
            signInErr.brand = ''
        }
        if(!status){
            flag = true
        }
        if (!files) {
            flag = false
            signInErr.imgFileName = "choose image"
        } else {
            signInErr.imgFileName = ""
            Object.keys(files).map((key, i) => {
                imgFileType.push(files[key].type)
                imgFileName.push(files[key].name)
                let type = files[key].type
                if (!type.match(/.(jpg|jpeg|png|gif)$/i)) {
                    flag = false
                    signInErr.files = "not image!"
                } else {
                    signInErr.files = ""
                }
            })
            if (files.length > 3) {
                flag = false
                signInErr.imgFileName = "max 3 pics"
            } else {
                signInErr.imgFileName = ''
            }
        }
        if (!flag) {
            this.setState({ formErrors: signInErr })
        } else {
            this.setState({ formErrors: [] })
            let url = 'http://localhost/laravel/laravel/public/api/user/add-product'
            let accessToken = userData.loginToken['token'];
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            let formData = new FormData();
            formData.append('name', this.state.name);
            formData.append('price', this.state.price);
            formData.append('category', this.state.category);
            formData.append('brand', this.state.brand);
            formData.append('company', this.state.company);
            formData.append('detail', this.state.detail);
            formData.append('status', this.state.status);
            formData.append('sale', this.state.sale);
            Object.keys(image).map((Obj, i) => {
                formData.append("file[]", image[Obj])
            });
            axios.post(url, formData, config)
                .then(res => {
                    console.log(res.data)
                    this.props.history.push('/account/product/list')
                })
                .catch(error => { console.log(error) })
        }
    }

    render() {
        return (
            <div className="col-sm-4">
                <div className="signup-form">
                    <h2>Create Product</h2>
                    <FormErrors formErrors={this.state.formErrors} />
                    <form action="#" onSubmit={this.handleSubmit} enctype="multipart/form-data">
                        <input type="text" name="name" placeholder="name" onChange={this.handleInput} />
                        <input type="number" placeholder="Price" name="price" onChange={this.handleInput} />
                        <select name="category" onChange={this.handleInput}>
                            <option>please choose category</option>
                            {this.renderCategory()}
                        </select>
                        <select name="brand" onChange={this.handleInput}>
                            <option value="0">please choose brand</option>
                            {this.renderBrand()}
                        </select>
                        <select name="status" onChange={this.handleInput}>
                            <option value="1">new</option>
                            <option value="2">sale</option>
                        </select>
                        {this.renderSale()}
                        <input type="text" placeholder="Company profile" name="company" onChange={this.handleInput} />
                        <input type="file" name="files[]" multiple onChange={this.handleUserInputFile} />
                        <textarea name="detail" onChange={this.handleInput} placeholder="Detail" />
                        <button type="submit" className="btn btn-default">Signup</button>
                    </form>
                </div>
            </div>
        )
    }
}
