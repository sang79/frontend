import React, { Component } from 'react';
import axios from 'axios';
import CartItem from './CartItem';
export default class Cart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            idSubtract: '',
            cart: []
        }
        this.renderCart = this.renderCart.bind(this)
        this.deleteProduct = this.deleteProduct.bind(this)
        this.subtract = this.subtract.bind(this)
    }
    componentDidMount() {
        let productCart = JSON.parse(localStorage.getItem('localCart'))
        axios.post('http://localhost/laravel/laravel/public/api/product/cart', productCart)
            .then(res => {
                const cart = res.data.data
                this.setState({ cart })
            })
            .catch(error => console.log(error))
    }
    deleteProduct(idDelete) {
        let {cart} = this.state
        cart = cart.filter(p => p.id != idDelete);
        this.setState({cart})
    }
    subtract(idSubtract) {
        let {cart } = this.state
        cart = this.state.cart.filter(p => p.id != idSubtract);
        this.setState({ cart })
    }
    renderCart() {
        let {cart} = this.state
        if (Object.keys(cart).length > 0) {
            return Object.keys(cart).map((key, i) => {
                let product = cart[key]
                return (
                    <CartItem key={i} product={product}  deleteProduct={this.deleteProduct} subtract={this.subtract} />
                )
            })
        }

    }
    render() {
        return (
            <div className="col-md-12">
                <section id="cart_items">
                    <div className="table-responsive cart_info">
                        <table className="table table-condensed">
                            <thead>
                                <tr className="cart_menu">
                                    <td className="image">Item</td>
                                    <td className="description"></td>
                                    <td className="price">Price</td>
                                    <td className="quantity">Quantity</td>
                                    <td className="total">Total</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                {this.renderCart()}
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        )
    }
}
