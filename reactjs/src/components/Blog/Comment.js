import React, { Component } from 'react';
import axios from 'axios';
import { Link, withRouter } from 'react-router-dom';
class Comment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id_blog: '',
            id_user: '',
            name_user: '',
            id_comment: '0',
            isSubCmt: '',
            comment: '',
            image_user: '',
            err: '',
            msg: '',
            errPost: {}
        };
        this.handlePost = this.handlePost.bind(this)
        this.handleInput = this.handleInput.bind(this)
    }
    handleInput(e) {
        const comment = e.target.name;
        const value = e.target.value;
        this.setState({
            comment: value
        })
    }
    componentDidMount() {
        console.log(this.props.idCmt)
    } 
    handlePost(e) {
        e.preventDefault();
        let userData = JSON.parse(localStorage.getItem('userData'));
        let comment = this.state.comment;
        let err = this.state.err;
        let errComment = this.state.errPost
        if (!userData) {
            alert('Please Login first!');
        } else {
            let paramId = this.props.match.params.id;
            let url = 'http://localhost/laravel/laravel/public/api/blog/comment/' + paramId
            let accessToken = userData.loginToken['token'];
             let config = { 
                headers: { 
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };
            const formData = new FormData();
            formData.append('id_blog', paramId);
            formData.append('id_user', userData.loginAuth.id);
            formData.append('id_comment', this.props.idCmt ? this.props.idCmt : 0);
            formData.append('comment', this.state.comment);
            formData.append('image_user', userData.loginAuth.avatar);
            formData.append('name_user', userData.loginAuth.name);
            axios.post(url, formData, config)
                .then(res => {
                    console.log(res)
                    this.props.listCmt(res.data.data)
                    if (res.data.errors) {
                        errComment = res.data.errors
                    } else {
                        this.setState({ msg: 'cmt success' })
                        this.setState({ comment: '' })
                    }
                })
                .catch(error => {
                    console.log(error)
                })
        }
        if (userData && !comment) {
            this.setState({
                err: 'please text your comment below!'
            })
        } else {
            this.setState({ err: '' })
        }
    }

    render() {
        return (
            <div className="replay-box">
                <div className="row">
                        <div className="col-sm-4"></div>
                        <div className="col-sm-8">
                            <div className="text-area">
                                <form id="cmt" onSubmit = {this.handlePost}>
                                <div className="blank-arrow">
                                    <label>Your Name</label>
                                </div>
                                <span>*</span>
                                <textarea name="comment" rows="11" onChange={this.handleInput} value={this.state.comment}></textarea>
                                <p>{this.state.err}</p>
                                <p>{this.state.msg}</p>
                                <button class= "btn btn-primary" type="submit">POST COMMENT</button>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
        )
    }
}
export default withRouter(Comment); 
