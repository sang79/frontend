import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import DeleteProduct from './DeleteProduct';
export default class myProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listProduct: [],
            deleteProduct: [],
        }
        this.renderProduct = this.renderProduct.bind(this)
        this.listId = this.listId.bind(this)
    }
    componentDidMount() {
        let url = 'http://localhost/laravel/laravel/public/api/user/my-product'  
        let userData = JSON.parse(localStorage.getItem('userData'));
        let accessToken = userData.loginToken['token'];
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        axios.get(url, config)
            .then(res => {
                console.log(res)
                const listProduct = res.data.data
                this.setState({ listProduct })
            })
            .catch(error => console.log(error))
    }
    listId(data) {
        this.setState(this.state.listProduct = data)
    }
    renderProduct() {
        let getProduct = this.state.listProduct;
        if (Object.keys(getProduct).length > 0) {
            return Object.keys(getProduct).map((key, i) => {
                let img = JSON.parse(getProduct[key]['image']);
                let idProduct = getProduct[key]['id']
                return (
                        <tbody index={i} key={i}>
                            <tr index={i}>
                                <td className="cart_description">
                                    <h4><p>{idProduct}</p></h4>
                                </td>
                                <td className="cart_description">
                                    <h4><p>{getProduct[key]['name']}</p></h4>
                                </td>
                                <td className="cart_product">
                                    <img style={{ width: "100px" }} src={"http://localhost/laravel/laravel/public/upload/user/product/" + getProduct[key]['id_user'] + "/" + img[0]} />
                                </td>
                                <td className="cart_quantity">
                                    <p className="cart_total_price">{"$" + getProduct[key]['price']}</p>
                                </td>
                                <td>
                                    <Link to={"/account/product/edit/" + getProduct[key]['id']} className="cart_quantity_delete" ><i class="fa fa-edit"></i></Link>
                                    <DeleteProduct listId={this.listId} idProduct={idProduct} />
                                </td>
                            </tr>
                        </tbody>
                )
            })
        }
    }
    render() {
        return (
            <div className="col-md-9">
                <section id="cart_items">
                    <div className="table-responsive cart_info">
                        <table className="table table-condensed">
                            <thead>
                                <tr className="cart_menu">
                                    <td className="description">Id</td>
                                    <td className="description">Name</td>
                                    <td className="image">Images</td>
                                    <td className="price">Price</td>
                                    <td className="total">Action</td>
                                </tr>
                            </thead>
                            {this.renderProduct()}
                        </table>
                    </div>
                    <Link to={"/account/product/add/"} className="btn btn-primary">Add New</Link>
                </section>
            </div>
        )
    }
}


