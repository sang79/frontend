import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import PopUp from './Popup';
import AddToCart from './AddToCart';

export default class ProductIndexDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imgName: '',
            dataDetail: []
        }
        this.renderProductDetail = this.renderProductDetail.bind(this)
        this.renderImages = this.renderImages.bind(this)
        this.handleImage = this.handleImage.bind(this)

    }
    componentDidMount() {
        let paramId = this.props.match.params.id
        axios.get('http://localhost/laravel/laravel/public/api/product/detail/' + paramId)
            .then(res => {
                const dataDetail = res.data.data
                this.setState({ dataDetail })
            })
            .catch(error => console.log(error))
    }
    handleImage(e) {
        e.preventDefault()
        let imgName = e.target.src
        this.setState({imgName})
    }
    renderImages() {
        let dataDetailList = this.state.dataDetail;
        let img = JSON.parse(dataDetailList['image']);
        if (img) {
            return img.map((Obj, i) => {
                return (
                    <div key={i} className="item active" style={{ display: 'inline-block', margin: '3px' }}>
                        <Link key={i} to="">
                            <img style={{ height: '60px' }}  key={i} src={"http://localhost/laravel/laravel/public/upload/user/product/" + dataDetailList['id_user'] + "/" + Obj} onClick={this.handleImage} />
                        </Link >
                    </div>
                )
            })
        }
    }
    renderProductDetail() {
        let imgName = this.state.imgName
        let dataDetailList = this.state.dataDetail;
        if (Object.keys(dataDetailList).length > 0) {
            // let img = JSON.parse(dataDetailList['image']);
            return (
                <div className="product-details">
                    <div className="col-sm-5">
                        <div className="view-product">
                            <img src={imgName} />
                            <PopUp popData={this.state.imgName} />
                        </div>
                        <div id="similar-product" className="carousel slide" data-ride="carousel">
                            <div className="carousel-inner">
                                {this.renderImages()}
                            </div>
                            <a className="left item-control" href="#similar-product" data-slide="prev">
                                <i className="fa fa-angle-left"></i>
                            </a >
                            <a className="right item-control" href="#similar-product" data-slide="next">
                                <i className="fa fa-angle-right"></i>
                            </a >
                        </div>
                    </div>
                    <div className="col-sm-7">
                        <div className="product-information">
                            <img src="images/product-details/new.jpg" className="newarrival" alt="" />
                            <h2>{dataDetailList.name}</h2>
                            <p>Web ID: 1089772</p>
                            <img src="images/product-details/rating.png" alt="" />
                            <span>
                                <span>{"$" + dataDetailList.price}</span>
                                <label>Quantity:</label>
                                <input type="text" value="3" />
                                <AddToCart idProduct={this.state.dataDetail['id']} />
                            </span>
                            <p><b>Availability:</b> In Stock</p>
                            <p><b>Condition:</b> New</p>
                            <p><b>Brand:</b> E-SHOPPER</p>
                            <Link to=""><img src="/../frontend/images/product-details/share.png" className="share img-responsive" alt="" /></Link >
                        </div>
                    </div>
                </div>
            )
        }
    }
    render() {
        return (
            <div className="col-sm-9 padding-right">
                {this.renderProductDetail()}
            </div>
        )
    }
}

