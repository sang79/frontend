import React, { Component } from 'react';
export default class AddToCart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            productCart: {},
            cart: {},
            quantity: 1,
        }
        this.handleCart = this.handleCart.bind(this)
    }
    handleCart(e) {
        let {productCart, cart, quantity} = this.state
        let id = this.props.idProduct
        this.setState({id})
        productCart[id] = this.state.quantity
        console.log(productCart)
        var getCart = JSON.parse(localStorage.getItem('localCart'))
        if(getCart) {
            Object.keys(getCart).map((key, i) => {
                console.log(key)
            })
        }
        localStorage.setItem('localCart', JSON.stringify(productCart))
    }
    render() {
        let id = this.props.idProduct;

        return (
            <button type="button" id={id} onClick={this.handleCart} className="btn btn-fefault cart">
                <i className="fa fa-shopping-cart"></i> Add to cart
            </button>
        )
    }

}
   
