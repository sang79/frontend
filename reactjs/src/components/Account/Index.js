import React, { Component } from 'react';
import {
  Switch,
  Route
} from "react-router-dom";
import App from './App'
import Update from './Member/Update'
import AddProduct from './Product/AddProduct';
import MyProduct from './Product/MyProduct'
import EditProduct from './Product/EditProduct'

class Index extends Component {
  render () {
    return (
      <App>
        <Switch>
          <Route path='/account/member' component={Update} />
          <Route path='/account/product/list' component={MyProduct} />
          <Route path='/account/product/add' component={AddProduct} />
          <Route path='/account/product/edit/:id' component={EditProduct} />
        </Switch>
      </App>
    )
  }
}
export default Index
