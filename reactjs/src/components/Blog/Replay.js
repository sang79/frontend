import React, { Component } from 'react';
import axios from 'axios'
export default class Replay extends Component {
    constructor(props) {
        super(props)
        this.state = { 
            subCmt: '',
            err: '',
            msg: ''
         }
        this.handleReplay = this.handleReplay.bind(this)
    }
    handleReplay(e) {
        e.preventDefault();
        let id_child = this.props.replay
        this.setState({id_child})
        let {subCmt } = this.state
        let errComment = this.state.err
        let userData = JSON.parse(localStorage.getItem('userData'));
        if(userData)
        console.log(this.props.infoReplay)
        let paramId = this.props.infoReplay.match.params.id;
            let url = 'http://localhost/laravel/laravel/public/api/blog/comment/' + paramId
            let accessToken = userData.loginToken['token'];
             let config = { 
                headers: { 
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };
            const formData = new FormData();
            formData.append('id_blog', paramId);
            formData.append('id_user', userData.loginAuth.id);
            formData.append('id_comment', this.state.id_child );
            formData.append('comment', this.state.subCmt);
            formData.append('image_user', userData.loginAuth.avatar);
            formData.append('name_user', userData.loginAuth.name);
            axios.post(url, formData, config)
                .then(res => {
                    console.log(res.data)
                    if (res.data.errors) {
                        errComment = res.data.errors
                    } else {
                        this.setState({ msg: 'cmt success' })
                        this.setState({ comment: '' })
                    }
                })
                .catch(error => {
                    console.log(error)
                })
    }
    render() {
        return (
            <a class="btn btn-primary" href="#cmt" onClick={this.handleReplay}><i class="fa fa-reply"></i>Replay</a >
        )
    }
}
