import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
class Index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            info: []
        }
    }
    componentDidMount() {
        axios.get('http://localhost/laravel/laravel/public/api/blog') 
            .then(res => {
                const info = res.data.blog.data;
                this.setState({ info });
            })
            .catch(error => console.log(error))
    }
    renderBlog() {
        let getBlog = this.state.info;
        if (getBlog.length > 0) {
            return getBlog.map((Obj, i) => {
                return (
                    <div key={i} index={i} className="single-blog-post">
                        <h3>{Obj.title}</h3>
                        <div className="post-meta">
                            <ul>
                                <li><i className="fa fa-user"></i> Mac Doe</li>
                                <li><i className="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i className="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <span>
                                <i className="fa fa-star"></i>
                                <i className="fa fa-star"></i>
                                <i className="fa fa-star"></i>
                                <i className="fa fa-star"></i>
                                <i className="fa fa-star-half-o"></i>
                            </span>
                            <Link to="">
                                <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/" + Obj.image} alt="" />
                            </Link>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <Link to={'/blog/detail/' + Obj.id} className="btn btn-primary">Read More</Link>
                        </div>
                    </div>
                )
            })
        }
    }

    render() {    
        return (
            <div className="col-sm-9">
                <div className="blog-post-area">
                        <h2 className="title text-center">Latest From our Blog</h2>
                        {this.renderBlog()}
                </div>
            </div>
        )
    }
}
export default Index;
