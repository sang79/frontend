import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import './index.css';
import App from './App';
import Home from './components/Layout/Home' 
import ProductIndex from './components/Products/ProductIndex'
import ProductIndexDetail from './components/Products/ProductIndexDetail'
import Account from './components/Account/Index'; 
import Login from './Member/Login';
import Index from './components/Blog/Index';
import Detail from './components/Blog/Detail';
import Cart from './components/Cart/Cart';
import reportWebVitals from './reportWebVitals';
ReactDOM.render(
  <div>
    <Router>
      <App>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/login' component={Login} />
          <Route path='/list' component={Index} />
          <Route path='/blog/detail/:id' component={Detail} />
          <Route path= '/product' component = {ProductIndex} />
          <Route path= '/productlist/detail/:id' component = {ProductIndexDetail} />
          <Route path='/cart' component={Cart} />
          <Route component={Account} />
        </Switch>
      </App>
    </Router>
  </div>,
  document.getElementById('root')
);

reportWebVitals();
