import React, { Component } from 'react';
import FormErrors from '../Member/FormErrors';
import axios from 'axios';
import { Link } from 'react-router-dom';
class Account extends Component {
 
    render() {
       
        return (
                <div className="col-sm-3">
                    <div className="left-sidebar">
                        <h2>Account</h2>
                        <div className="panel-group category-products" id="accordian">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h4 className="panel-title">
                                        <Link data-toggle="collapse" data-parent="#accordian" to="/account/update">
                                            <span className="badge pull-right"><i className="fa fa-plus"></i></span>
											Account
										</Link >
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div className="panel-group category-products" id="accordian">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h4 className="panel-title">
                                        <Link data-toggle="collapse" data-parent="#accordian" to="">
                                            <span className="badge pull-right"><i className="fa fa-plus"></i></span>
											My Products
										</Link >
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             
        )
    }
}
export default Account;
