import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
export default class ProductIndex extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            quantity: 1,
            productCart: {},
            dataProduct: []
        }
        this.handleCart = this.handleCart.bind(this)
        this.renderProduct = this.renderProduct.bind(this)
    }
    componentDidMount() {
        axios.get('http://localhost/laravel/laravel/public/api/product')
            .then(res => {
                const dataProduct = res.data.data
                this.setState({ dataProduct })
                console.log(dataProduct)
            })
            .catch(error => console.log(error))
    }
   
    handleCart(e) {
        e.preventDefault()
        let getId = e.target.id;
        let productCart = {};
        let flag = true;
        let getCart = localStorage.getItem('localCart')
        if (getCart) {
            productCart = JSON.parse(getCart)
            Object.keys(productCart).map((key, i) => {
                if (key == getId) {
                    productCart[key] = productCart[key] + 1;
                    flag = false;
                }
            })
        }
        if (flag == true) {
            productCart[getId] = 1;
        }
        console.log(Object.keys(productCart).length)
        localStorage.setItem('localCart', JSON.stringify(productCart))

    }
    renderProduct() {
        let dataProductList = this.state.dataProduct
        if (dataProductList.length > 0) {
            return dataProductList.map((Obj, i) => {
                let img = JSON.parse(Obj['image']);
                return (
                    <div className="col-sm-4" key={i} index={i}>
                        <div className="product-image-wrapper">
                            <div className="single-products">
                                <div className="productinfo text-center">
                                    <img src={"http://localhost/laravel/laravel/public/upload/user/product/" + Obj.id_user + "/" + img[0]} />
                                    <h2>{"$" + Obj.price}</h2>
                                    <p>{Obj.name}</p>
                                    <button type="button" className="btn btn-fefault cart">
                                        <i className="fa fa-shopping-cart"></i> Add to cart</button>

                                </div>
                                <div className="product-overlay">
                                    <div className="overlay-content">
                                        <h2>{"$" + Obj.price}</h2>
                                        <p>{Obj.name}</p>
                                        <a onClick={this.handleCart} id={Obj.id} className="btn btn-fefault cart">
                                            <i className="fa fa-shopping-cart"></i> Add to cart</a>
                                    </div>
                                </div>
                            </div>
                            <div className="choose">
                                <ul className="nav nav-pills nav-justified">
                                    <li><Link to=""><i className="fa fa-plus-square"></i>Add to wishlist</Link ></li>
                                    <li><Link to={"/productlist/detail/" + Obj.id}><i className="fa fa-plus-square"></i>More</Link ></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                )
            })
        }
    }
    render() {
        return (
            <div className="col-sm-9 padding-right">
                <div className="features_items">
                    <h2 className="title text-center">Features Items</h2>
                    {this.renderProduct()}
                </div>
            </div>
        )
    }
}
