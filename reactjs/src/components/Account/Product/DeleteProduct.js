import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios'
export default class DeleteProduct extends Component {
    constructor(props) {
        super(props)
        this.state = { listDeleteProduct: [] }
        this.deleteProduct = this.deleteProduct.bind(this)
    }
    componentDidMount() {
        console.log(this.props)
    }
    deleteProduct() {
        let deleteEachProduct = this.props.idProduct
        console.log(deleteEachProduct)
        if (deleteEachProduct) {
            let url = 'http://localhost/laravel/laravel/public/api/user/delete-product/' + deleteEachProduct
            let userData = JSON.parse(localStorage.getItem('userData'));
            let accessToken = userData.loginToken['token'];
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            axios.get(url, config)
                .then(res => {
                    let idProduct = res.data
                    this.setState({ idProduct })
                    this.props.listId(res.data.data)
                })
                .catch(error => console.log(error))
        }
    }


    render() {
        return (
            <a className="cart_quantity_delete" onClick={this.deleteProduct} >
                <i className="fa fa-times"></i>
            </a>
        )
    }
}   
