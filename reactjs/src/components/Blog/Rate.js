import React, { Component } from 'react';
import StarRatings from 'react-star-ratings';
import axios from 'axios';
class Rate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      rate: 0,
      user_id: '',
      blog_id: ''
    }
    this.changeRating = this.changeRating.bind(this)
  }
  changeRating(newRating, name) {
    this.setState({
      rate: newRating
    });

    let userData = JSON.parse(localStorage.getItem('userData'));
    if (userData) {
      let paramId = this.props.details.match.params.id;
      let url = 'http://localhost/laravel/laravel/public/api/blog/rate/' + paramId
      let accessToken = userData.loginToken['token'];
      let config = {
        headers: {
          'Authorization': 'Bearer ' + accessToken,
          'Content-Type': 'application / x - www - form - urlencoded',
          'Accept': 'application / json'
        }
      };
      const formData = new FormData();
      formData.append('user_id', userData.loginAuth.id);
      formData.append('blog_id', paramId);
      formData.append('rate', this.state.rate);
      axios.post(url, formData, config)
        .then(res => {
        })
        .catch(error => {
          console.log(error)
        })
    } else {
      alert('login first!')
    }
  }
  componentDidMount() {
    let paramId = this.props.details.match.params.id;
    axios.get('http://localhost/laravel/laravel/public/api/blog/rate/' + paramId)
      .then(res => {
        let rating = res.data.data
        let userNumber = Object.keys(rating).length
        if (userNumber > 0) {
          let sumRating = 0;
          Object.keys(rating).map((keys, value) => {
            sumRating = sumRating + rating[keys].rate
          })
          let userRating = sumRating / userNumber
          this.setState({
            rate: userRating
          })
        } 
      })
  }
  render() {
    return (
      <StarRatings
        rating={this.state.rate}
        starRatedColor="blue"
        changeRating={this.changeRating}
        numberOfStars={5}
        name='rating'
      />
    );
  }
}
export default Rate;
