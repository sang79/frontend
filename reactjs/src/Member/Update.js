import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FormErrors from '../Member/FormErrors';
import axios from 'axios';
export default class Update extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            email: '',
            password: '',
            address: '',
            country: '',
            phone: '',
            avatar: '',
            file: '',
            level: '0',
            formErrors: {},
            msg: ''
        }
        this.handleInput = this.handleInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUserInputFile = this.handleUserInputFile.bind(this)
    }

    handleInput(e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameInput]: value
        })
    }
    handleUserInputFile(e) {
        const file = e.target.files
        let reader = new FileReader();
        reader.onload = (e) => {
            this.setState({
                avatar: e.target.result,
                file: file[0]
            })
        }
        reader.readAsDataURL(file[0])
    }

    handleSubmit(e) {
        e.preventDefault();
        let userData = JSON.parse(localStorage.getItem('userData'));
        let { userName, email, password, address, country, phone, avatar } = this.state
        let flag = true;
        if (flag && userData) {
            this.setState({
                msg: 'ok',
                formErrors: {}
            })
            let url = 'http://localhost/laravel/laravel/public/api/user/update/' + userData.loginAuth.id
            let accessToken = userData.loginToken['token'];
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application / x - www - form - urlencoded',
                    'Accept': 'application / json'
                }
            };
            const formData = new FormData();
            formData.append('name', userName);
            formData.append('email', userData.loginAuth.email);
            formData.append('phone', phone);
            formData.append('password', password);
            formData.append('address', address);
            formData.append('avatar', this.state.avatar);
            formData.append('country', country);
            formData.append('level', 0);
            axios.post(url, formData, config)
                .then(res => {
                    console.log(res.data)
                    let userData = {
                        loginAuth: res.data['Auth'],
                        loginToken: res.data['success']
                    }
                    localStorage['userData'] = JSON.stringify(userData)
                })
                .catch(error => { console.log(error) })
        }
    }
    componentDidMount() {
        let userData = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : {};
        this.setState({
            userName: userData.loginAuth.name,
            email: userData.loginAuth.email,
            userName: userData.loginAuth.name,
            country: userData.loginAuth.country,
            address: userData.loginAuth.address,
            phone: userData.loginAuth.phone,
            avatar: userData.loginAuth.avatar
        })
    }
    render() {
        return (
            <>
                <div className="col-sm-3">
                    <div className="left-sidebar">
                        <h2>Account</h2>
                        <div className="panel-group category-products" id="accordian">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h4 className="panel-title">
                                        <Link data-toggle="collapse" data-parent="#accordian" to="/account">
                                            <span className="badge pull-right"><i className="fa fa-plus"></i></span>
                                        Account
                                    </Link >
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div className="panel-group category-products" id="accordian">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h4 className="panel-title">
                                        <Link data-toggle="collapse" data-parent="#accordian" to="/product">
                                            <span className="badge pull-right"><i className="fa fa-plus"></i></span>
                                        My Products
                                    </Link >
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="col-sm-4"></div>,
                <div className="col-sm-4">
                    <div className="signup-form">
                        <h2>User Update!</h2>
                        <p>{this.state.msg}</p>
                        <FormErrors formErrors={this.state.formErrors} />
                        <form action="#" onSubmit={this.handleSubmit} enctype="multipart/form-data">
                            <input type="text"  name="userName" value = {this.state.userName} onChange={this.handleInput}  />
                            <input type="email" value = {this.state.email} name="email" onChange={this.handleInput} />
                            <input type="password" name="password" onChange={this.handleInput} />
                            <input type="text" value = {this.state.address} name="address" onChange={this.handleInput} />
                            <input type="text" value = {this.state.country} name="country" onChange={this.handleInput} />
                            <input type="number" value = {this.state.phone} name="phone" onChange={this.handleInput} />
                            <input type="file" name="avatar" onChange={this.handleUserInputFile} />
                            <button type="submit" className="btn btn-default">Signup</button>
                        </form>
                    </div>
                </div>
          
            </>
        )
    }
}
 
