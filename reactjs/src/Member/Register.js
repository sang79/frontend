import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import FormErrors from '../Member/FormErrors';
import axios from 'axios';
class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            email: '',
            password: '',
            address: '',
            country: '',
            phone: '',
            avatar: '',
            file: '',
            level: '0',
            formErrors: {},
            msg: ''
        }
        this.handleInput = this.handleInput.bind(this)
        this.handleSubmitRegister = this.handleSubmitRegister.bind(this)
        this.handleUserInputFile = this.handleUserInputFile.bind(this)
    }
    handleUserInputFile(e) {
        const file = e.target.files;
        let reader = new FileReader();
        reader.onload = (e) => {
            this.setState({
                avatar: e.target.result,
                file: file[0]
            });
        }
        reader.readAsDataURL(file[0])
    }
    handleInput(e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameInput]: value
        })
    }
    handleSubmitRegister(e) {
        e.preventDefault();
        let { file } = this.state
        console.log(file)
        let size = file.size
        let type = file.type
        let flag = true;
        let submitErrors = this.state.formErrors;
        let { userName, email, password, address, country, phone, avatar } = this.state
        if (!email) {
            flag = false
            submitErrors.email = "enter email"
        } else {
            submitErrors.email = ''
        }
        if (!userName) {
            flag = false
            submitErrors.userName = "enter your name"
        } else {
            submitErrors.userName = ''
        }
        if (!password) {
            flag = false
            submitErrors.password = "enter password"
        } else {
            submitErrors.password = ''
        }
        if (!address) {
            flag = false
            submitErrors.address = "enter your address"
        } else {
            submitErrors.address = ''
        }
        if (!country) {
            flag = false
            submitErrors.country = "enter your country"
        } else {
            submitErrors.country = ''
        }
        if (!phone) {
            flag = false
            submitErrors.phone = "enter your phone"
        } else {
            submitErrors.phone = ''
        }
        if (file && (size > (1024 * 1024))) {
            flag = false
            submitErrors.file = "another size!"
        } else {
            submitErrors.file = ""
        }
        if (file && !type.match(/.(jpg|jpeg|png|gif)$/i)) {
            flag = false
            submitErrors.file = "not an image!"
        }else {
            submitErrors.file = ''
        }
        if (!avatar) {
            flag = false
            submitErrors.avatar = 'pick your avatar'
        } else {
            submitErrors.avatar = ''
        }
        if (!flag) {
            this.setState({
                formErrors: submitErrors
            })
        } else {
            this.setState({
                formErrors: {}
            })
            let user = {
                name: userName,
                avatar: this.state.avatar,
                password: this.state.password,
                email: this.state.email,
                address: this.state.address,
                country: this.state.country,
                phone: this.state.phone,
                level: 0
            }
            axios.post('http://localhost/laravel/laravel/public/api/register', user)
                .then(res => {
                    console.log(res);
                    if (res.data.errors) {
                        this.setState({
                            formErrors: res.data.errors
                        })
                    } else {
                        this.setState({
                            msg: "register success! Please login"
                        })
                       
                    }
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }
    render() {
        return (
            <div className="col-sm-4">
                <div className="signup-form">
                    <h2>New User Signup!</h2>
                    <p>{this.state.msg}</p>
                    <FormErrors formErrors={this.state.formErrors} />
                    <form action="" onSubmit={this.handleSubmitRegister} enctype="multipart/form-data">
                        <input type="text" placeholder="Name" name="userName" value = {this.state.value} onChange={this.handleInput} />
                        <input type="email" placeholder="Email Address" name="email" value = {this.state.value} onChange={this.handleInput} />
                        <input type="password" placeholder="Password" name="password" value = {this.state.value} onChange={this.handleInput} />
                        <input type="text" placeholder="Address" name="address" value = {this.state.value} onChange={this.handleInput} />
                        <input type="text" placeholder="Country" name="country" value = {this.state.value} onChange={this.handleInput} />
                        <input type="number" placeholder="Phone" name="phone" value = {this.state.value} onChange={this.handleInput} />
                        <input type="file" name="avatar" value = {this.state.value} onChange={this.handleUserInputFile} />
                        <button type="submit" className="btn btn-default" >Signup</button>
                    </form>
                </div>
            </div>
        )
    }
}
export default withRouter(Register);

