import React, { Component } from 'react';
import Replay from './Replay'
class ListComment extends Component {
	constructor(props) {
		super(props)
		this.state = {
			err: '',
			msg: ''
		}
		this.handleReplay = this.handleReplay.bind(this)
	}
	handleReplay(e) {
		const id_child = e.target.id;
		console.log(id_child)
		this.props.IdReplay(id_child)
	}
	renderComment() {
		let userInfo = this.props.listCmt
		console.log(userInfo)
		if (userInfo instanceof Array) {
			return userInfo.map((Obj1, i) => {
				if (Obj1.id_comment == 0) {
					return (
						<ul className="media-list" key={i}>
							<li className="media">
								<a className="pull-left">
									<img className="media-object" src={"http://localhost/laravel/laravel/public/upload/user/avatar/" + Obj1.image_user} alt="" />
								</a >
								<div className="media-body">
									<ul className="sinlge-post-meta">
										<li><i className="fa fa-user"></i>{Obj1.name_user}</li>
										<li><i className="fa fa-clock-o"></i> 1:33 pm</li>
										<li><i className="fa fa-calendar"></i> DEC 5, 2013</li>
									</ul>
									<p>{Obj1.comment}</p>
									<p className="text-muted">{Obj1.created_at}</p>
									<a id={Obj1.id} className="btn btn-primary" href="#cmt" onClick={this.handleReplay}><i className="fa fa-reply"></i>Replay</a >
								</div>
							</li>
							{userInfo.map((Obj2, j) => {
								if (Obj1.id == Obj2.id_comment) {
									return (
										<li className="media second-media" key={j} index={j}>
											<a className="pull-left">
												<img className="media-object" src={"http://localhost/laravel/laravel/public/upload/user/avatar/" + Obj2.image_user} alt="" />
											</a>
											<div className="media-body">
												<ul className="sinlge-post-meta">
													<li><i className="fa fa-user"></i>{Obj2.name_user}</li>
													<li><i className="fa fa-clock-o"></i> 1:33 pm</li>
													<li><i className="fa fa-calendar"></i> DEC 5, 2013</li>
												</ul>
												<p>{Obj2.id}</p>
												<p>{Obj2.comment}</p>
												<p className="text-muted">{Obj2.created_at}</p>
												<a id={Obj2.id} className="btn btn-primary" href="#cmt" onClick={this.handleReplay}><i className="fa fa-reply"></i>Replay</a >
											</div>
										</li>
									)
								}
							})}
						</ul>
					)
				}
			})
		}
	}
	render() {
		return (
			<div className="response-area">
				<h2>RESPONSES</h2>
				{this.renderComment()}
			</div>
		)
	}
}
export default ListComment;

