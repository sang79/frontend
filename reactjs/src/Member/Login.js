import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FormErrors from './FormErrors';
import Register from './Register' 
import axios from "axios";
class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            level: '0',
            password: '',
            msg: '',
            formErrors: {}
        }
        this.handleInput = this.handleInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInput(e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameInput]: value
        })
    }
    handleSubmit(e) {
        e.preventDefault();
        let { email, password } = this.state;
        let flag = true;
        let submitErrors = this.state.formErrors;

        if (!email) {
            flag = false
            submitErrors.email = "enter email"
        } else {
            submitErrors.email = ''
        }
        if (!password) {
            flag = false
            submitErrors.password = "enter password"
        } else {
            submitErrors.password = ''
        }
        if (!flag) {
            this.setState({
                formErrors: submitErrors
            })
        } else {
            this.setState({
                formErrors: {}
            })
            let user = {
                email: this.state.email,
                password: this.state.password,
                level: 0
            }
            axios.post('http://localhost/laravel/laravel/public/api/login', user)
                .then(res => {
                    if (res.data.errors) {
                        this.setState({
                            formErrors: res.data.errors
                        })
                    } else {
                        this.setState({
                            msg: "Login success"
                        })
                        let userData = {
                            loginAuth: res.data['Auth'],
                            loginToken: res.data['success']
                        }
                        localStorage['userData'] = JSON.stringify(userData)
                        this.props.history.push('/')
                    }
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }
    render() {
        return (
            <section id="form">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-4 col-sm-offset-1">
                            <div className="login-form">
                                <h2>Login to your account</h2>
                                <p>{this.state.msg}</p>
                                <FormErrors formErrors={this.state.formErrors} />
                                <form action="#" onSubmit={this.handleSubmit}>
                                    <input type="email" placeholder="Email Address" name="email" onChange={this.handleInput} />
                                    <input type="password" placeholder="password" name="password" onChange={this.handleInput} />
                                    <span>
                                        <input type="checkbox" className="checkbox" />
                    Keep me signed in
                </span>
                                    <button type="submit" className="btn btn-default">Login</button>
                                </form>
                            </div>
                        </div>
                        <div className="col-sm-1">
                            <h2 className="or">OR</h2>
                        </div>
                    <Register />
                    </div>
                </div>
            </section>
        )
    }
}
export default Login;














